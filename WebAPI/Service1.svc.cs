﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using System.ServiceModel.Channels;
using System.Diagnostics;
namespace WebAPI
{
    // メモ: [リファクター] メニューの [名前の変更] コマンドを使用すると、コード、svc、および config ファイルで同時にクラス名 "Service1" を変更できます。
    // 注意: このサービスをテストするために WCF テスト クライアントを起動するには、ソリューション エクスプローラーで Service1.svc または Service1.svc.cs を選択し、デバッグを開始してください。
    public class Service1 : IService1
    {
        public Message GetData(string value)
        {

            Hashtable ret = new ConsoleApplication1.Program().Kaiseki(new String[1]{value}, false);
            ArrayList al = new ArrayList();
            
            foreach(string val in ret.Values){
                al.Add(val);
            }
            string[] sdim;
            sdim = (string[])al.ToArray(typeof(string));
           // sdim = new String[2] {"a\\","b" };

            string body = JsonConvert.SerializeObject(sdim);

            Debug.WriteLine(body); 
            return WebOperationContext.Current.CreateTextResponse(body,
        "application/json; charset=utf-8",
        Encoding.UTF8);
        }
        public Message GetData2(string value)
        {

            Hashtable ret = new ConsoleApplication1.Program().Kaiseki(new String[1] { value }, false);
            ArrayList al = new ArrayList();

            foreach (string val in ret.Values)
            {
                al.Add(val);
            }
            string[] sdim;
            sdim = (string[])al.ToArray(typeof(string));
            // sdim = new String[2] {"a\\","b" };

            string body = JsonConvert.SerializeObject(sdim);

            Debug.WriteLine(body);
            return WebOperationContext.Current.CreateTextResponse(body,
        "application/json; charset=utf-8",
        Encoding.UTF8);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
