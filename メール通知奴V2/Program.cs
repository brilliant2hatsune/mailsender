﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections;
using メール通知奴;
using System.Reflection;
using System.Net.Mail;
using System.Web.Mail;
using System.Collections.Generic;


namespace ConsoleApplication1
{
    public class Program
    {
        
        static void Main(string[] args)
        {

            //from console
            new Program().Kaiseki(args, true);
            
        }

        public Hashtable Kaiseki(string[] args, Boolean isMail)
        {
            string senariofile = @"c:\test.txt";
            if (args.Length > 0)
            {
                senariofile = args[0];
            }
            Logger.setLog(System.IO.Path.GetDirectoryName(senariofile) + "\\" + System.IO.Path.GetFileNameWithoutExtension(senariofile) + ".log");

            Logger.log("シナリオファイル:" + senariofile);

            Dictionary<string, string> ParseDic = new Dictionary<string, string>();
            using (StreamReader sr = new StreamReader(senariofile, Encoding.GetEncoding("Shift_JIS")))
            {

                int loopcount = 0;
                Hashtable result = new Hashtable();
                ReqData req;
                HTTPMethod meth = new HTTPMethod();

                string[] ope = sr.ReadLine().Split('\t');

                req = new ReqData("", ope[0], ope[1], ope[2], ope[3], ope[4], ope[5], null);

                while (sr.Peek() >= 0)
                {

                    loopcount++;
                    // ファイルを 1 行ずつ読み込む
                    string buff = sr.ReadLine();
                    // オペレーション解析
                    ope = buff.Split('\t');
                    if (ope[1] != "")
                        req.URL = ope[1];
                    if (ope.Length > 2 && ope[2] != "")
                        req.UserAgent = ope[2];
                    if (ope.Length > 3 && ope[3] != "")
                        req.Accept = ope[3];
                    if (ope.Length > 4 && ope[4] != "")
                        req.Referer = ope[4];
                    if (ope.Length > 5 && ope[5] != "")
                        req.ContentType = ope[5];
                    if (ope.Length > 6 && ope[6] != "")
                        req.Host = ope[6];
                    if (ope.Length > 7 && ope[7] != "")
                        req.Encorder = Encoding.GetEncoding(ope[7]);

                    Hashtable vals = new Hashtable();
                    while (sr.Peek() >= 0)
                    {
                        buff = sr.ReadLine();
                        if (buff.StartsWith("-"))
                        {
                            req.Params = vals;
                            break;
                        }
                        else
                        {
                            int colpos = buff.IndexOf(':');
                            string key = "", value = "";
                            key = buff.Substring(0, colpos);
                            if (buff.Length > colpos)
                                value = buff.Substring(colpos + 1);
                            if (value.IndexOf('$') == 0)
                                value = ParseDic[value.Substring(1)];
                            vals[key] = value;
                        }

                    }

                    if (ope[0] == "post")
                        result[loopcount] = meth.Post(req);
                    else if (ope[0] == "get")
                        result[loopcount] = meth.Get(req);
                    else if (ope[0] == "parse")
                    {
                        loopcount--;
                        string beforeResult = (string)result[loopcount];
                        string key = ope[1];
                        int start = beforeResult.IndexOf(ope[2]) + ope[2].Length;
                        int end = beforeResult.IndexOf(ope[3], start);
                        string value = beforeResult.Substring(start, end - start);
                        ParseDic.Remove(key);
                        ParseDic.Add(key, value);
                        Logger.log("ParseKey:" + key + " ParsedValue:" + value);

                    }

                    Logger.log((string)result[loopcount]);

                    //通信処理完了
                    if (buff.StartsWith("-END-"))
                        break;

                }

                string[] libname = sr.ReadLine().Split('.');
                //指定の解析エンジンを実行
                Assembly asm = Assembly.Load(libname[0]);
                Type myType = asm.GetType(libname[0] + "." + libname[1]);
                MethodInfo myMethod = myType.GetMethod(libname[2]);
                object obj = Activator.CreateInstance(myType);
                Hashtable mailData = (Hashtable)myMethod.Invoke(obj, new object[] { result });

                ArrayList mailList = new ArrayList();

                //発信メール設定を取得
                string[] MailSetting = sr.ReadLine().Split('\t');

                //メール一覧の取得
                while (sr.Peek() >= 0)
                    mailList.Add(sr.ReadLine());

                if (isMail)
                    new MailSend().send((string)mailData["title"], (string)mailData["body"], mailList, MailSetting[0], Int32.Parse(MailSetting[1]), MailSetting[2] == "true" ? true : false, MailSetting[3], MailSetting[4]);

                return mailData;
            }
        }
    }
}