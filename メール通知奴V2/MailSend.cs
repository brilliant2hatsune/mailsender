﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace メール通知奴
{
    class MailSend
    {
        public void send(string title, string message, ArrayList maillist, string smtpServer, int port, bool useSSL, string user, string password)
        {
            using (System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient())
            {
                using (System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage()) { 
                    msg.From = new System.Net.Mail.MailAddress(((string)maillist[0]).Split('\t')[0], ((string)maillist[0]).Split('\t')[1]);
                    
                    for (int i = 1; i < maillist.Count; i++)
                        msg.To.Add(new System.Net.Mail.MailAddress(((string)maillist[i]).Split('\t')[0], ((string)maillist[i]).Split('\t')[1]));
                    
                    msg.Subject = title;

                    msg.IsBodyHtml = true;
                    msg.Body = message;
                
                    //SMTPサーバーなどを設定する
                    sc.Host = smtpServer;
                    sc.Port = port;
                    sc.Credentials = new System.Net.NetworkCredential(user, password);
                    
                    sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    
                    //ユーザー名とパスワードを設定する
                    //SSLを使用する
                    sc.EnableSsl = useSSL;
                    //メッセージを送信する
                    sc.Send(msg);
                    Logger.log("メール送信！");
                    Logger.log("to:" + msg.To.ToString());
                    Logger.log("title:" + msg.Subject + "\nBody:" + msg.Body);
                }
            }
        }
    }
}
