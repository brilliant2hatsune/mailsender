﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace メール通知奴
{
    class HTTPMethod
    {
        public String Get(ReqData reqData)
        {
            //パラメタ組立
            string param = "";
            if (reqData.Params != null)
            {
                param += "?";
                foreach (string k in reqData.Params.Keys)
                    param += String.Format("{0}={1}&", k, reqData.Params[k]);
            }
            reqData.URL += param;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(reqData.URL);
            req.UserAgent = reqData.UserAgent;
            req.CookieContainer = reqData.Cookie;
            req.Accept = reqData.Accept;
            req.Host = reqData.Host;
            req.Referer = reqData.Referer;

            WebResponse res = req.GetResponse();
            reqData.Referer = reqData.URL;//リファラ更新
            //レスポンス読み込む
            using (Stream resStream = res.GetResponseStream())
            using (StreamReader sr = new StreamReader(resStream, reqData.Encorder))
                return sr.ReadToEnd();
        }

        public String Post(ReqData reqData)
        {
            
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(reqData.URL);

            //パラメタを"param1=value1&param2=value2"の形にまとめる
            string param = "";
            foreach (string k in reqData.Params.Keys)
            {
                param += String.Format("{0}={1}&", k, reqData.Params[k]);
            }
            byte[] data = Encoding.ASCII.GetBytes(param);

            req.Method = "POST";
            req.UserAgent = reqData.UserAgent;
            req.Accept = reqData.Accept;
            req.Host = reqData.Host;
            req.Referer = reqData.Referer;
            req.ContentType = reqData.ContentType;
            req.ContentLength = data.Length;
            req.CookieContainer = reqData.Cookie;

                ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(
                        delegate
                        { return true; }
                   );
            

            //POSTを実行
            using (Stream reqStream = req.GetRequestStream())
                reqStream.Write(data, 0, data.Length);

            //HTTP GETによるクッキーの取得
            //GET実行
            WebResponse res = req.GetResponse();
            reqData.Referer = reqData.URL;//リファラ更新
            //レスポンス読み込む
            using (Stream resStream = res.GetResponseStream())
            using (StreamReader sr = new StreamReader(resStream, reqData.Encorder))
                return sr.ReadToEnd();
        }
    }
}
