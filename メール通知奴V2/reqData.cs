﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace メール通知奴
{
    class ReqData
    {
        public CookieContainer Cookie{get; set;}
        public String URL { get; set; }
        public String UserAgent { get; set; }
        public String Accept { get; set; }
        public String Referer { get; set; }
        public String Host { get; set; }
        public Hashtable Params { get; set; }
        public String ContentType { get; set; }
        public Encoding Encorder { get; set; }
        
        public ReqData(String URL, String UserAgent, String Accept, String Referer, String ContentType, String Host, String Encord, Hashtable Params)
        {
            this.URL = URL;
            Cookie = new CookieContainer();
            this.UserAgent = UserAgent;
            this.Accept = Accept;
            this.Referer = Referer;
            this.Params = Params;
            this.ContentType = ContentType;
            this.Host = Host;
            this.Encorder = Encoding.GetEncoding(Encord);
        }
    }
}
