﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Collections;


namespace ZeroSIM使用量パース
{
    public class Class1
    {
        public Hashtable doParse(Hashtable result) {
            string html = (string)result[3];
            //HTMlパース
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(@"//dl[@class=""useConditionDisplay""]//dt");
            string title1 = nodes[0].InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace(" ", "");
            string title2 = nodes[1].InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace(" ", "");

            nodes = doc.DocumentNode.SelectNodes(@"//dl[@class=""useConditionDisplay""]//dd");
            string usedSize1 = nodes[0].InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace(" ", "");
            string usedSize2 = nodes[1].InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace(" ", "");
            string planname = doc.DocumentNode.SelectNodes(@"//dl[@class=""planDisplay""]//dd")[0].InnerHtml;
            result = new Hashtable();
            result["title"] = DateTime.Now.ToString() + "現在:" + usedSize2;
            result["body"] = "<dl>" + planname + "</dl>" + title1 + "</br>" + usedSize1 + "<br/><br/>" + title2 + "</br>" + usedSize2 + "<br/><br/>by メール通知奴v3≪作：とし子≫";

            return result;
        
        }

    }
}
