﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Collections;


namespace おうちコープ注文一覧パース
{
    public class Class1
    {
        public Hashtable doParse(Hashtable result) {
            string html = (string)result[2];
            //HTMlパース
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(@"//div[@id=""title""]//h3");
            string title = nodes[0].InnerText;

            nodes = doc.DocumentNode.SelectNodes(@"//div[@id=""body_1""]//tr");

            //(4)<a>コレクションから値を取り出し
            html = "～ " + title + " ～<br/><br/>確定した注文は以下となりまする<br/><table>\n<tr>";
            foreach (HtmlNode node in nodes)
            {
                //テキストをセット
                html += node.InnerHtml + "</tr>";

                //リンクをセット
                //str = node.Attributes["href"].Value + "\r\n";
            }
            html += "</table><br/><br/>以上でござる。";
            
            result = new Hashtable();
            result["title"] = "[おうちコープ奴] " + title;
            result["body"] = html;

            return result;
        
        }

        public Hashtable doParse2(Hashtable result)
        {
            string html = (string)result[3];
            //HTMlパース
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(@"//div[@class=""mhd""]//h2");
            string title = "≪事前通知≫" + nodes[0].InnerText.Replace("\r","").Replace("\n","").Replace("\t","") ;
            
            nodes = doc.DocumentNode.SelectNodes(@"//div[@class=""details_date bx""]");
            string shimekiri = nodes[0].InnerText;
           
            //(4)<a>コレクションから値を取り出し
            html = title + "<br/><br/>以下が注文済みとなっておりまする。買い忘れはござらぬか？<br/><br/>" + shimekiri + "<br/><br/>";
            nodes = doc.DocumentNode.SelectNodes(@"//table");

            foreach (HtmlNode node in nodes)
            {
                //テキストをセット
                html += "<table>" + node.InnerHtml + "</table><br/><br/>";

            }

            html += "以上でござる。";

            result = new Hashtable();
            result["title"] = "[おうちコープ奴] " + title;
            result["body"] = html;

            return result;

        }
    }
}
